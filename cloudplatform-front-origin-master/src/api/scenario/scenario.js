import ApiServeice from '~/api/config/api.service';

const scenarioApi = {
getScenarioList: (params) => {
return ApiServeice.get('/api/scenario/list', { params });
},
getScenarioById: (id) => {
    return ApiServeice.get(`/api/scenario/${id}`);
    }
};
export default scenarioApi;