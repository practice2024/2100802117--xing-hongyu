import axios from 'axios'
import JwtService from '~/api/config/jwt.service'
// import Swal from "sweetalert2";
import store from '~/store'

import { ElMessageBox } from 'element-plus'
const errorCode = {
  '401': '认证失败，无法访问系统资源',
  '403': '当前操作没有权限',
  '404': '访问资源不存在',
  default: '系统未知错误,请反馈给管理员'
}
// 创建axios的一个实例
var ApiService = axios.create({
  baseURL: '/neuedu', // 接口统一域名
  timeout: 10000 // 设置超时
})
ApiService.defaults.baseURL = '/neuedu'
ApiService.defaults.headers.post['Content-Type'] =
  'application/json;charset=utf-8'
ApiService.defaults.headers.common['Cache-Control'] = 'no-cache'
ApiService.defaults.headers.common['Accept'] = '*/*'
// ------------------- 一、请求拦截器 忽略
ApiService.interceptors.request.use(
  function (config) {
    // 为每个请求设置token
    if (JwtService.getToken()) {
      config.headers['Authorization'] = 'Bearer ' + JwtService.getToken()
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

/**
 * Send the GET HTTP request
 * @param resource
 * @param slug
 * @returns {*}
 */
ApiService.query = (resource, slug = '') => {
  return ApiService.get(`${resource}/${slug}`).catch(error => {
    throw new Error(`ApiService ${error}`)
  })
}

/**
 * Send the UPDATE HTTP request
 * @param resource
 * @param slug
 * @param params
 * @returns {IDBRequest<IDBValidKey> | Promise<void>}
 */
ApiService.update = (resource, slug, params) => {
  return ApiService.put(`${resource}/${slug}`, params)
}

// ----------------- 二、响应拦截器 忽略
ApiService.interceptors.response.use(
  res => {
    // 未设置状态码则默认成功状态
    const code = res.data.code || 200
    // 获取错误信息
    const msg = errorCode[code] || res.data.msg || errorCode['default']

    if (code === 401) {
      ElMessageBox.alert('登录状态已过期，请重新登录', '错误信息', {
        confirmButtonText: '重新登录',
        type: 'error',
        showClose: false,
        buttonSize: 'default',
        cancelButtonClass: 'msg-cancel-btn',
        confirmButtonClass: 'msg-confirm-btn neu-bg-red neu-border-red neu-color-white',
        customClass: 'neu-error-box icho-message-box',
        callback: () => {
          store.dispatch('LOGOUT').then(() => {
            location.reload() // 为了重新实例化vue-router对象 避免bug
          })
        }
      })
      return res.data
    } else if (code === 500) {
      ElMessageBox.alert(
        msg || '系统未知错误,请反馈给管理员',
        '错误信息',
        {
          showClose: false,
          buttonSize: 'default',
          customClass: 'neu-error-box icho-message-box',
          confirmButtonClass: 'neu-bg-red neu-border-red neu-color-white',
          type: 'error',
          confirmButtonText: '关闭'
        }
      )
      return Promise.reject(new Error(msg))
    } else if (code === 100) {
      return res.data
    } else if (code !== 200) {
      ElMessageBox.alert(
        msg || '系统未知错误,请反馈给管理员',
        '错误信息',
        {
          showClose: false,
          type: 'error',
          buttonSize: 'default',
          customClass: 'neu-error-box icho-message-box',
          confirmButtonClass: 'neu-bg-red neu-border-red neu-color-white',
          confirmButtonText: '关闭'
        }
      )
      return Promise.reject(new Error(msg))
    } else {
      return res.data
    }
  },
  error => {
    let { message } = error
    if (message === 'Network Error') {
      message = '后端接口连接异常'
    } else if (message.includes('timeout')) {
      message = '系统接口请求超时'
    } else if (message.includes('Request failed with status code')) {
      if (message.substr(message.length - 3) === '431') {
        message = '请求参数超长请重试!'
      } else {
        message = '系统接口' + message.substr(message.length - 3) + '异常'
      }
    }
    ElMessageBox.alert(
      message || '系统未知错误,请反馈给管理员',
      '错误信息',
      {
        showClose: false,
        buttonSize: 'default',
        type: 'error',
        customClass: 'neu-error-box icho-message-box',
        confirmButtonClass: 'neu-bg-red neu-border-red neu-color-white',
        confirmButtonText: '关闭'
      }
    )
    return Promise.reject(error)
  }
)

export default ApiService
