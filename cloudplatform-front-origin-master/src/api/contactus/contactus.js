import ApiService from '~/api/config/api.service'
const contact = {
 getOrgList: () => {
 return ApiService.get('/api/orgnization/list')
 }, 
 sendMessage: (message) => {
 return ApiService.post('/api/message',message)
 }
}
export default contact;