import ApiServeice from '~/api/config/api.service';

const serveApi = {
  getServeList: (params) => {
    return ApiServeice.get('/api/serve/list', { params });
  },
getServeById: (id) => {
        return ApiServeice.get(`/api/serve/${id}`);
      },
};export default serveApi;