import ApiService from '~/api/config/api.service'
const menu = {
  getRouters: () => {
    return ApiService.get('/system/menu/getRouters')
  },

  /* 获取全菜单列表*/
  getFullMenus: () => {
    return ApiService.get('/api/article/list')
  },
  getTenantMenus () {
    return ApiService.get('/system/menu/queryAdminMenuList')
  },
  getOtherTenantMenus () {
    return ApiService.get('/system/menu/queryOtherRoleMenuList')
  },
  /* 懒加载菜单列表*/
  getPartMenus: params => {
    return ApiService.get('/system/menu/list', { params: params })
  },
}

// 获取路由
export default menu
