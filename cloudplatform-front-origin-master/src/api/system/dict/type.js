import ApiService from '~/api/config/api.service'

// 查询字典类型列表
export function listType (params) {
  return ApiService.get('/system/dict/type/list', { params: params })
}

// 查询字典类型详细
export function getType (dictId) {
  return ApiService.get(`/system/dict/type/${dictId}`)
}


// 获取字典选择框列表
export function optionselect () {
  return ApiService.query('/system/dict/type/optionselect')
}
