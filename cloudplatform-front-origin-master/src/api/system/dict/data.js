import ApiService from '~/api/config/api.service'

// 查询字典数据列表
export function listData (params) {
  return ApiService.get('/system/dict/data/list', { params: params })
}

// 查询字典数据详细
export function getData (dictCode) {
  return ApiService.get(`/system/dict/data/${dictCode}`)
}

// 根据字典类型查询字典数据信息
export function getDicts (dictType) {
  return ApiService.get('/system/dict/data/type/' + dictType)
}
