import ApiServeice from '~/api/config/api.service';
const downloadApi = {
    getDownloadList: (params) => {
        return ApiServeice.get('/api/download/list', { params });
    }
};
export default downloadApi;