import { createStore } from 'vuex'
import auth from './auth.module'
import size from './size.module'
import permission from './permission.module'

export default createStore({
  modules: {
    auth,
    size,
    permission
  }
})
