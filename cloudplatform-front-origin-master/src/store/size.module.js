
const state = {
  bodyWidth: 0,
  bodyMinWidth: 0,
  bodyHeight: 0,
  headerHeight:64,
  footerHeight:200
}

const getters = {
  bodyWidth(state) {
    return state.bodyWidth
  },
  bodyHeight(state) {
    return state.bodyHeight
  },
  headerHeight(state) {
    return state.headerHeight
  },
  footerHeight(state) {
    return state.footerHeight
  },
}

const actions = {}

const mutations = {
  changeOffsetHei(state, size) {
    state.bodyWidth = size.offsetWid || 0
    state.bodyMinWidth = size.offsetWid > 1280 ? size.offsetWid : 1280
    state.bodyHeight = size.offsetHei > 768 ? size.offsetHei : 768
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
