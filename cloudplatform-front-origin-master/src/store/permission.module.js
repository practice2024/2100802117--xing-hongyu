
/* eslint-disable */
import menu from '~/api/system/menu.js'

const permission = {
  state: {
    keepAliveList: [],
    routes: [],
    addRoutes: [],
    stuCTLoading:false,
    stuCTLoadingExit:false
  },
  getters: {
    routes(state) {
      return state.routes;
    },
    // 动态路由
    addRoutes(state) {
      return state.addRoutes;
    },
    keepAliveList(state){
      return state.keepAliveList
    },
    stuCTLoading(state){
      return state.stuCTLoading
    },
    stuCTLoadingExit(state){
      return state.stuCTLoadingExit
    },
  },
  mutations: {
    SET_ROUTES: (state, routes) => {
      let dashboard = []
      state.addRoutes = dashboard.concat(
        filterHiddenRouter(JSON.parse(JSON.stringify(routes)))
      );
      state.routes = dashboard.concat(routes);
    },
    CHANGE_ALIVE_LIST(state,routes){
      state.keepAliveList = getAliveRouter(routes,['/'])
    },
    CHANGE_LOADING_STATUS(state, status = false){
      state.stuCTLoading = !!status
    },
    CHANGE_LOADING_EXIT_STATUS(state, status = false){
      state.stuCTLoadingExit = !!status
    },
  },
  actions: {
    // 生成路由
    GENERATE_ROUTES(state) {
      return new Promise(resolve => {
        // 向后端请求路由数据
        menu.getRouters().then(res => {
          const accessedRoutes =[...res.data] 
          state.commit('CHANGE_ALIVE_LIST',accessedRoutes)
          state.commit("SET_ROUTES", accessedRoutes);
          resolve(accessedRoutes);
        });
      });
    }
  }
};

// // 将所有隐藏的动态路由过滤掉
function filterHiddenRouter(routers) {
  return routers.filter(route => {
    if (route.children != null && route.children && route.children.length) {
      route.children = filterHiddenRouter(route.children);
    }
    return !route.hidden;
  });
}

function getAliveRouter(routers,list=[]) {
  let _list = [...list]
  routers.forEach(route => {
    if(route.name && route.meta.noCache){
      _list.push(route.name)
    }
    if (route.children != null && route.children && route.children.length) {
      let $list = [..._list]
      _list = getAliveRouter(route.children, $list);
    }
  });
  return _list
}

export default permission;

/* eslint-enable */
