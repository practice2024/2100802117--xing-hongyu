import ApiService from '~/api/config/api.service'
import JwtService from '~/api/config/jwt.service'
import axios from 'axios'

const state = {
  errors: null,
  // user: JwtService.hasUser() ? JwtService.getUser() : {},
  user: {},
  isAuthenticated: JwtService.hasToken(),
  roles: [],
  permissions: []
}

const rawAxios = axios.create({
  baseURL: '/neuedu',
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json'
  }
})
const getters = {
  currentUser (state) {
    return state.user
  },
  isAuthenticated (state) {
    return state.isAuthenticated
  },
  roles (state) {
    return state.roles
  },
  avatar (state) {
    return state.user.user ? state.user.user.avatar : ''
  },
  userId (state) {
    return state.user.user ? state.user.user.userId : ''
  },
  userName (state) {
    return state.user.user ? state.user.user.userName : ''
  },
  nickName (state) {
    return state.user.user ? state.user.user.nickName : ''
  },
  role (state) {
    return state.user.roles && state.user.roles.length > 0 ? state.user.roles[0] : ''
  },
  basePath (state) {
    let _role = state.user.roles && state.user.roles.length > 0 ? state.user.roles[0] : ''
    let _path = _role === 'supadmin' ? '/system' : _role === 'admin' ? '/tenant' : _role === 'student' ? '/student' : '/teacher'
    return _path
  },
  permissions (state) {
    return state.permissions
  }
}

const actions = {
  LOGIN (context, credentials) {
    return new Promise((resolve, reject) => {
      rawAxios
        .post('/auth/login', credentials)
        .then(res => {
          var data = res.data
          if (data.code === 200) {
            context.commit('SET_AUTH', { token: data.data.access_token })
          } else {
            context.commit('SET_ERROR', [res.data.msg])
          }
          resolve(data)
        })
        .catch(error => {
          context.commit('SET_ERROR', [error.message])
          reject(error)
        })
    })
  },
  LOGOUT (context) {
    if (JwtService.hasToken()) {
      return new Promise((resolve, reject) => {
        rawAxios.defaults.headers['Authorization'] = 'Bearer ' + JwtService.getToken()
        rawAxios.delete('/auth/logout').then(res => {
          if (res.data.code === 200) {
            context.commit('PURGE_AUTH')
            context.commit('SET_ROLES', [])
            context.commit('SET_PERMISSIONS', [])
          }
          rawAxios.defaults.headers['Authorization'] = ''
          resolve(res.data)
        })
          .catch(error => {
            rawAxios.defaults.headers['Authorization'] = ''
            reject(error)
          })
      })
    }
  },
  VERIFY_AUTH (context) {
    !JwtService.hasToken() && context.commit('PURGE_AUTH')
  },
  UPDATE_USER (context) {
    return new Promise((resolve, reject) => {
      ApiService.get('/system/user/getInfo')
        .then(data => {
          if (data.roles && data.roles.length > 0) {
            // 验证返回的roles是否是一个非空数组
            context.commit('SET_ROLES', data.roles)
            context.commit('SET_PERMISSIONS', data.permissions)
          } else {
            context.commit('SET_ROLES', ['ROLE_DEFAULT'])
          }

          context.commit('SET_AUTH', {
            user: data,
            token: JwtService.getToken()
          })
          resolve(data)
        })
        .catch(err => {
          context.commit('SET_ERROR', [err.message])
          reject(err)
        })
    })
  }
}

const mutations = {
  SET_ERROR (state, error) {
    state.errors = error
  },
  SET_AUTH (state, data) {
    state.isAuthenticated = true
    state.user = !data.user || !Object.keys(data.user).length ? {} : data.user
    state.errors = {}
    JwtService.saveToken(!data.token ? '' : data.token)
    JwtService.saveUser(state.user)
  },
  PURGE_AUTH (state) {
    state.isAuthenticated = false
    state.user = {}
    state.errors = {}
    JwtService.clearStorage()
  },
  SET_ROLES (state, roles) {
    state.roles = roles
  },
  SET_PERMISSIONS (state, permissions) {
    state.permissions = permissions
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
