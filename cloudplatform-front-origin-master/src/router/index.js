import { createRouter, createWebHistory } from 'vue-router'
import jwtService from '~/api/config/jwt.service'
import store from '~/store'

// 首页用重定向的方式来进行适配的方案
let isMobile = /Android|webos|iphone|iPod|BlackBerry|liPad/i.test(navigator.userAgent)
let redirectPath = isMobile ? '/m_portal' : '/p_portal/mainContent';

const router = createRouter({
  history: createWebHistory(),
  routes: [
     {
      path: '/',
      redirect: redirectPath,
      name: 'index',
      component: () => import('~/views/layout/Layout.vue'),
    },
    // PC端页面
    {
      name: 'p_portal',
      path: '/p_portal',
      meta: {
        title: '门户',
        type: 'pc'
      },
      
      component: () => import('~/views/pc_portal/index.vue'),
      children: [
        {
          name: 'p_scenario',
          path: '/p_portal/scenario',
          meta: {
            title: '产教融合博览会',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/scenario.vue')
        },
        {
          name: 'p_serve',
          path: '/p_portal/serve',
          meta: {
            title: '产教融合博览会',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/serve.vue')
        },
        {
          name: 'p_newsCenter',
          path: '/p_portal/newsCenter',
          meta: {
            title: '新闻中心',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/newsCenter.vue')
        },
        {
          name: 'p_noticeAnnouncements',
          path: '/p_portal/noticeAnnouncements',
          meta: {
            title: '通知公告',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/noticeAnnouncements.vue')
        },
        {
          name: 'p_dataDownload',
          path: '/p_portal/dataDownload',
          meta: {
            title: '资料下载',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/dataDownload.vue')
        },
        {
          name: 'p_contactUs',
          path: '/p_portal/contactUs',
          meta: {
            title: '联系我们',
            type: 'pc'
          },
          component: () => import('~/views/pc_portal/content/contactUs.vue')
        },
        // {
        //   name: 'userCenter',
        //   path: '/p_portal/userCenter',
        //   meta: {
        //     title: '个人中心',
        //     type: 'pc'
        //   },
        //   component: () => import('~/views/pc_portal/content/userCenter/index.vue')
        // }
      ]
    },
    {
      name: 'p_portal_home',
      path: '/p_portal/mainContent',
      meta: {
        title: '门户',
        type: 'pc'
      },
      component: () => import('~/views/pc_portal/home.vue'),
    },
    {
      name: '404',
      path: '/404',
      meta: {
        title: '404'
      },
      component: () => import('~/views/error/ErrorPage.vue')
    }
  ]
})


// 七朵云相关的页面路由 
const sevenCloudRoutes = [
  {
    name: 'p_scenarioDetail',
    path: '/p_portal/cloudSub/detail',
    meta: {
      title: '详情',
      type: 'pc'
   
    },
    component: () => import('~/views/pc_portal/content/cloudSubPages/detail.vue')
  },
]

// 将七朵云相关的页面路由添加进router 
sevenCloudRoutes.forEach(r=>{
  router.addRoute(r)
})

/* eslint-disable */
router.beforeEach((to, from, next) => {
  if (/Android|webos|iphone|iPod|BlackBerry|liPad/i.test(navigator.userAgent) && to.meta.type === 'pc') {
    next({ path: to.path.split('p_').join('m_') })

  } else if (!/Android|webos|iphone|iPod|BlackBerry|liPad/i.test(navigator.userAgent) && to.meta.type === 'mobile') {
    next({ path: to.path.split('m_').join('p_') })
  } else {
    next()
  }
})
function addRouteNow ($router, list, parentName = "index", basePath = '') {
  // 循环添加可访问路由表
  if (list && list.length > 0) {
    list.forEach(item => {
      let List = item.children || []
      if (item.name) {
        if (item.component && item.component !== 'ParentView' && item.component !== 'Layout')
          $router.addRoute(
            parentName, {
            ...changeItem(item, basePath)
          })
        if (List.length > 0) {
          addRouteNow($router, List, 'index')
        }
      }
      else {
        // 实验过程返回参数逻辑 ================需调整
        if (item.path === '/') {
          if (List.length === 1) {
            addRouteNow($router, List, 'index', item.path)
          } else {
            addRouteNow($router, List, 'index')
          }
        }
      }
    });
  }
}
const modules = import.meta.glob('../views/pages/*/*/*.vue')
const AddRouters = import.meta.glob('../views/layout/AddRouters.vue')
function changeItem (item, basePath = '') {
  let _item = {
    ...item
  }
  _item.meta.keepAlive = _item.meta.noCache
  // let itemUrl = `../views/${_item.component === 'Layout' ? 'layout/AddRouters.vue' : 'pages' + _item.component}`
  // _item.component = _item.component&&_item.component!=='ParentView'? (() => import(itemUrl)) : null
  let itemCom = _item.component === 'Layout' ? AddRouters['../views/layout/AddRouters.vue'] : modules['../views/pages' + _item.component]
  _item.component = _item.component && _item.component !== 'ParentView' ? itemCom : null
  _item.path = basePath + _item.path
  return _item
}
/* eslint-enable */
export default router