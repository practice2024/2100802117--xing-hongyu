
import axios from 'axios'
import JwtService from '~/api/config/jwt.service'
import ApiService from '~/api/config/api.service'
import { getDicts } from '/src/api/system/dict/data'

const baseConfigUrl = process.env.VITE_VIDEO_API_DOMAIN
const baseConfigVideoUrl = process.env.VITE_VIDEO_SLICE_DOMAIN || '/slices'
const uploadAxios = axios.create({
  baseURL: baseConfigUrl
})
const uploadVideoAxios = axios.create({
  baseURL: baseConfigVideoUrl
})
const fileObject = {
  getDicts,
  async previewVideo (fileId) {
    let src = ''
    await uploadVideoAxios //
      .get(`/slices/getFilePreViewByEid/${fileId}`)
      .then(res => {
        src = res.data.data
      })
      .catch(() => { })

    return src
  },

  getFileContent (fileId) {
    return uploadAxios.get('/content/s3/getObject?eid=' + fileId, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + JwtService.getToken()
      }
    })
  },
  // 获取文件连接，供img标签等使用
  getFileUrl (fileId) {
    return fileId
      ? baseConfigUrl.concat(
        '/content/s3/previewObject?eid=',
        fileId
      )
      : ''
  },
  // 获取文件连接，供video切片播放使用
  getVideoUrl (fileId) {
    return fileId
      ? baseConfigUrl.concat(
        '/content/video?fileId=',
        fileId
      )
      : ''
  },
  // 获取文件连接，供下载使用
  getDownLoadFileUrl (fileId) {
    return fileId
      ? baseConfigUrl.concat(
        '/content/s3/getObject?eid=',
        fileId
      )
      : ''
  },

  /**
   * 获取导出模板file流文件
   *
   * @param fileId
   * @returns {*}
   */
  downloadFiles (fileId, filename) {
    return uploadAxios
      .get('/resource/download/importTemplate', {
        params: { fileId: fileId },
        responseType: 'blob',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(stream => {
        fileObject.storageFile(stream, filename)
      })
      .catch(() => { })
  },

  /**
   * 获取file流文件下载
   *
   * @param fileId
   * @returns {*}
   */
  downloadFilesToLocal (fileId, filename) {
    return uploadAxios
      .get('/content/s3/getObject', {
        params: { eid: fileId, projectTag: 'ct' },
        responseType: 'blob',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(stream => {
        fileObject.storageFile(stream, filename)
      })
      .catch(() => { })
  },
  /**
   * 将文件流存储成文件
   *
   * @param stream
   * @param filename
   */
  storageFile (stream, filename) {
    const blob = new Blob([stream])
    if ('download' in document.createElement('a')) {
      const elink = document.createElement('a')
      elink.download = filename
      elink.style.display = 'none'
      elink.href = URL.createObjectURL(blob)
      document.body.appendChild(elink)
      elink.click()
      URL.revokeObjectURL(elink.href)
      document.body.removeChild(elink)
    } else {
      navigator.msSaveBlob(blob, filename)
    }
  },
  accepts: {
    SP: {
      accept: ['.mp4'],
      hint: '仅支持 mp4 媒体格式'
    },
    YP: {
      accept: ['.mp3', '.ogg'],
      hint: '仅支持 mp3，ogg 媒体格式'
    },
    WD: {
      accept: ['.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.pdf', '.txt'],
      hint: '仅支持doc，docx，xls，xlsx，ppt，pptx，pdf，txt文档格式'
    },
    TP: {
      accept: ['.jpg', '.png', '.jpeg', '.gif', '.bmp'],
      hint: '仅支持 jpg，png，jpeg，gif，bmp 文档格式'
    },
    QTZY: {
      accept: ['.zip', '.rar'],
      hint: '仅支持 zip，rar 压缩格式'
    },
    MD: {
      accept: ['.md'],
      hint: '仅支持 md 文件格式'
    },
    WZ: {
      accept: ['*'],
      hint: ''
    },
  }
}

export default fileObject
