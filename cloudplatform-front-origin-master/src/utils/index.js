import {
  messageBox,
  message,
  selectDictLabel,
  getPlainText,
  fileXss
} from './util.js'
export default [
  {
    name: 'mmsgBox',
    fnc: messageBox
  },
  {
    name: 'mmsg',
    fnc: message
  },
  {
    name: 'selectDictLabel',
    fnc: selectDictLabel
  },
  {
    name:'getPlainText',
    fnc:getPlainText
  },
  {
    name:'fileXss',
    fnc:fileXss
  }
]
