/**
 * 通用js方法封装处理
 * Copyright (c) 2019
 */
import { ElMessage } from 'element-plus'
// xss 过滤
import xss from "xss";
// 回显数据字典
export function selectDictLabel (datas, value) {
  var actions = []
  Object.keys(datas).some(key => {
    if ('' + datas[key].dictValue === '' + value) {
      actions.push(datas[key].dictLabel)
      return true
    }
  })
  return actions.join('')
}


export async function messageBox (config) {
  let res = false
  let actions = config.actions || []
  let textTrue = ''
  let textFalse = ''
  let _type = config.type || config.variant || 'info'
  let _theme = config.theme || 'light'
  if (actions.length > 0) {
    let listTrue = config.actions.filter(t => t.key === 'true')
    textTrue = listTrue.length > 0 ? listTrue[0].text : ''
    let listFalse = config.actions.filter(t => t.key === 'false')
    textFalse = listFalse.length > 0 ? listFalse[0].text : ''
  }
  await this[config.msgType ? '$' + config.msgType : '$confirm'](
    config.text,
    config.title || '提示信息',
    {
      showClose: false,
      closeOnClickModal: false,
      closeOnPressEscape: false,
      draggable: true,
      buttonSize:'default',
      dangerouslyUseHTMLString: config.isHtml || false,
      customClass: `neu-${_type}-box icho-message-box neu-${_theme}-theme-message`,
      cancelButtonClass: 'msg-cancel-btn',
      confirmButtonClass: _theme ==='dark'?'msg-confirm-btn dark-btn':`msg-confirm-btn  neu-btn-bg-${_type === 'error' ? 'red' : 'blue'} neu-border-${_type === 'error' ? 'red' : 'blue'}`,
      confirmButtonText: config.confirmButtonText || textTrue || '确认',
      cancelButtonText: config.cancelButtonText || textFalse || '取消',
      type: config.type
        ? config.type
        : config.variant
          ? config.variant
          : _type
    }
  )
    .then(() => {
      res = true
      if (config.callback) {
        config.callback()
      }
    })
    .catch(() => {
      if (config.callbackError) {
        config.callbackError()
      }
    })
  return Promise.resolve(res)
}

/**
 * 消息提示
 * config: {
 *     text, 消息内容
 *     type, 类型
 *     before<Function>, 消息提示前调用
 *     after<Function>, 消息提示后调用
 * }
 * @param config
 */
export function message (config) {
  config &&
    config.before &&
    setTimeout(
      config.before(),
      config.beforetimeout ? config.beforetimeout : 0
    )
  ElMessage({
    message: config.text ? config.text : '',
    duration: config.timeout ? config.timeout : 1500,
    type: config.type
      ? config.type
      : config.variant
        ? config.variant
        : 'success'
  })
  config &&
    config.after &&
    setTimeout(config.after, config.aftertimeout ? config.aftertimeout : 1400)
}

export function checkSpecificKey (str) {
  var specialKey = "[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'"
  for (var i = 0; i < str.length; i++) {
    if (specialKey.indexOf(str.substr(i, 1)) !== -1) {
      return false
    }
  }
  return true
}

/**
 * 只能输入【字母(大小写)、数字、中划线、下划线】的30位字符串
 * @param {string} str
 * @returns {Boolean}
 */
export function isDigital (str) {
  const reg = /^[0-9a-zA-Z/_-]+$/
  return reg.test(str)
}

/**
 * 用户名校验
 * @param {string} str
 * @returns {Boolean}
 */

export function isName (str) {
  const reg = /^[a-zA-Z\u4e00-\u9fa5][a-zA-Z0-9_\u4E00-\u9FA5]{1,30}$/;
  return reg.test(str);
}

/**
 * 密码必须由数字、字母、特殊字符组合,请输入6-14位'
 * @param {string} str
 * @returns {Boolean}
 */
export function isPassword (str) {
  const reg = /^(?![a-zA-z]+$)(?!\d+$)(?![~!@#$%^&*()_+-=.`]+$)(?![a-zA-z\d]+$)(?![a-zA-z!@#$%^&*]+$)(?![~!@#$%^&*()_+-=.`]+$)[a-zA-Z~!@#$%^&*()_+-=.`]{6,14}$/
  return reg.test(str)
}

/**
 * 正整数校验
 * @param {string} str
 * @returns {Boolean}
 */
export function isPositiveInteger (str) {
  const reg = /^\d+$/;
  return reg.test(str);
}


//转意符换成普通字符
function convertIdeogramToNormalCharacter(val) {
  const arrEntities = {'lt':'<','gt':'>','nbsp':' ','amp':'&','quot':'"'};
  return val.replace(/&(lt|gt|nbsp|amp|quot);/ig,function(all,t){return arrEntities[t];});
}

// 获取富文本的纯文字内容
export const getPlainText = (richCont) => {
  const str = richCont;
  let value = richCont;
  if(richCont){
    // 方法一： 
    value= value.replace(/(您的浏览器不支持 video 标签。)/g, "");  //去除video的特殊展示文字
    value= value.replace(/\s*/g, "");  //去掉空格
    value= value.replace(/<[^>]+>/g, ""); //去掉所有的html标记
    value = value.replace(/↵/g, "");     //去掉所有的↵符号
    value = value.replace(/[\r\n]/g, "") //去掉回车换行
    value = value.replace(/&nbsp;/g, "") //去掉空格
    value = convertIdeogramToNormalCharacter(value);
    return value;

    // 方法二： 
    // value = value.replace(/(\n)/g, "");
    // value = value.replace(/(\t)/g, "");
    // value = value.replace(/(\r)/g, "");
    // value = value.replace(/<\/?[^>]*>/g, "");
    // value = value.replace(/\s*/g, "");
    // value = convertIdeogramToNormalCharacter(value);
    // return value;
  } else {
    return null;
  }
}

export const fileXss = new xss.FilterXSS({
  onIgnoreTagAttr: function (tag, name, value, isWhiteAttr) {
    if (name.substr(0, 5) === 'data-' || name==='class' || name==='id'|| name==='color'|| name==='height'|| name==='width'|| name==='style') {
      // 通过内置的escapeAttrValue函数来对属性值进行转义
      return name + '="' + xss.escapeAttrValue(value) + '"';
    }
  },
  whiteList:{
    a: ['href', 'title', 'target','onclick'],
    p: ['class'],
    div: ['style','class'],
    b: ['style'],
    i: ['style'],
    br:['style'],
    source:['src','type'],
    span: ['style','class'],
    svg: ['style','width','height','viewBox','xmlns'],
    img:['src','style','width','height','class'],
    video:['src','style','width','height','class','controls','poster'],
    u:['style'],
    strike:['style'],
    h1:['style'],
    h2:['style'],
    h3:['style'],
    h4:['style'],
    h5:['style'],
    h6:['style'],
    font:['color','style', 'size'],
    section:['style'],
    footer:['style'],
    header:['style'],
    table:['style'],
    thead:['style'],
    th:['style'],
    tr:['style'],
    td:['style'],
    tbody:['style'],
    tfoot:['style'],
    map:['style'],
    hr:['style'],
    area:['style'],
    big:['style'],
    em:['style'],
    small:['style'],
    strong:['style'],
    sub:['style'],
    sup:['style'],
    ins:['style'],
    del:['style'],
    code:['style'],
    kbd:['style'],
    samp:['style'],
    tt:['style'],
    var:['style'],
    pre:['style'],
    abbr:['style'],
    address:['style'],
    blockquote:['style'],
    dfn:['style'],
    q:['style'],
    cite:['style'],
    center:['style'],
    col:['style'],
    colgroup:['style'],
    ol:['style'],
    ul:['style'],
    li:['style'],
    dl:['style'],
    dt:['style'],
    dd:['style']
  }
})