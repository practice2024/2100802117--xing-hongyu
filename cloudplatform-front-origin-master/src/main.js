import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import directiveList from '~/config/directive.js'
import echarts from '~/config/echart.js'
import fileObject from '~/config/file.js'
import moment from '~/config/moment.js'
import i18n from '~/locales/index'  // 引入

import utilsObject from '~/utils/index.js'

import IchooaComponents from '~/components/init'
import * as ELIconModules from '@element-plus/icons-vue'

// 引入全局样式 包括element复写样式
import "~/styles/index.scss";

import "element-plus/theme-chalk/src/index.scss"
import ElementPlus from 'element-plus'

// 视频预览插件
import vue3videoPlay from '/public/vue3-video-play/index.es.js'
import '/public/vue3-video-play/style.css'

// animate
//import animate from 'animate.css'
// anime
import anime from 'animejs/lib/anime.js';

const app = createApp(App)
// element svg icon
for (let iconName in ELIconModules) {
  app.component(iconName, ELIconModules[iconName])
}

// 自定义指令
directiveList.forEach(element => {
  app.directive(element.name, element.obj)
})

// 全局方法
app.config.globalProperties.fileObject = fileObject
app.config.globalProperties.echarts = echarts
app.config.globalProperties.anime = anime
app.config.globalProperties.moment = moment
utilsObject.forEach(element => {
  app.config.globalProperties[element.name] = element.fnc
})
app.use(router)
  .use(store)
  .use(IchooaComponents)
  .use(i18n)
  .use(ElementPlus, { zIndex: 1900, size: 'default' })
  .use(vue3videoPlay)
  //.use(animate)
  .mount('#app')
