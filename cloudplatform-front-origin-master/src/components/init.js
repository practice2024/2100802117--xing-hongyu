import ElPaginationNewComponent from './ElPaginationNew/index.vue'
import ElSelectNewComponent from './ElSelectNew/index.vue'

import ImgUploadComponent from './Upload/image.vue'
import UploadFiles from './Upload/uploadFiles.vue'
import MarkdownUploadComponent from './Upload/markdown.vue'
import MarkdownEditComponent from './Upload/mdEdit.vue'

import PreviewImage from './Preview/image.vue'
import PreviewVideo from './Preview/video.vue'
import PreviewAudio from './Preview/audio.vue'
import PreviewPdf from './Preview/pdf.vue'
import PreviewMd from './Preview/markdown.vue'
import PreviewPdfNoDraw from './Preview/pdfNoDraw.vue'
import PreviewResource from './Preview/previewResource.vue'
import TextEditor from './TextEditor/wangEditor.vue'

import ListPage from './ListPage/index.vue'
import ComPage from './common/commonPage.vue'

import LoadingPage from './dialogCom/loadingPage.vue'

const IchooaComponents = {
  install: function (Vue) {
    Vue.component('ElPaginationNew', ElPaginationNewComponent)
    Vue.component('ElSelectNew', ElSelectNewComponent)
    Vue.component('ImgUpload', ImgUploadComponent)
    Vue.component('UploadFiles', UploadFiles)
    Vue.component('MarkdownUploadComponent', MarkdownUploadComponent)
    Vue.component('MarkdownEditComponent', MarkdownEditComponent)
    Vue.component('PreviewImage', PreviewImage)
    Vue.component('PreviewVideo', PreviewVideo)
    Vue.component('PreviewAudio', PreviewAudio)
    Vue.component('PreviewPdf', PreviewPdf)
    Vue.component('PreviewMd', PreviewMd)
    Vue.component('PreviewPdfNoDraw', PreviewPdfNoDraw)
    Vue.component('PreviewResource', PreviewResource)
    Vue.component('TextEditor', TextEditor)
    Vue.component('ListPage', ListPage)
    Vue.component('ComPage', ComPage)
    Vue.component('LoadingPage', LoadingPage)
  }
}

export default IchooaComponents
