// 引入 wangEditor
import E from 'wangeditor'
// import Wangeditor from "../../../components/TextEditor/wangEditor";

const { $, BtnMenu } = E

export class FileUploadMenu extends BtnMenu{
    constructor(editor) {
        const $elem = E.$(
            `<div class="w-e-menu" data-title="附件" style="font-size: 20px" xmlns="http://www.w3.org/1999/html">
                    <div style="width: 20px;height: 20px;display: flex;">
                    <input id="upload-file" type="file" style="opacity: 0;width: 20px;height: 20px;position: absolute;" onchange="handleFileChange(this)"/>
                    <svg  t="1654923930486" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1331" width="20" height="20"><path d="M448 876.8 806.4 512c128-128 57.6-262.4 0-313.6-57.6-57.6-192-128-320 0L172.8 512c-89.6 89.6-38.4 185.6 0 224 38.4 38.4 134.4 89.6 224 0l313.6-313.6c25.6-25.6 32-51.2 32-64 0-38.4-25.6-64-32-70.4-25.6-25.6-83.2-51.2-134.4 0L262.4 601.6c-12.8 12.8-12.8 32 0 44.8 12.8 12.8 32 12.8 44.8 0l313.6-313.6c19.2-19.2 32-12.8 44.8 0 0 0 25.6 19.2 0 44.8l-313.6 313.6c-64 64-121.6 12.8-134.4 0-12.8-12.8-64-70.4 0-134.4l313.6-313.6c108.8-108.8 217.6-12.8 224 0 12.8 12.8 108.8 115.2 0 224L403.2 832l0 0c-12.8 12.8-12.8 32 0 44.8C416 889.6 435.2 889.6 448 876.8L448 876.8 448 876.8C448 876.8 448 876.8 448 876.8 448 876.8 448 876.8 448 876.8z" p-id="1332" fill="#707070"></path></svg>
                    </div>
                </div>`
        )
        // data-title属性表示当鼠标悬停在该按钮上时提示该按钮的功能简述
        super($elem, editor)
    }
    // 菜单点击事件
    clickHandler() {
        // 做任何你想做的事情
        // 可参考【常用 API】文档，来操作编辑器

        // alert('hello world')
    }
    // 菜单是否被激活（如果不需要，这个函数可以空着）
    // 1. 激活是什么？光标放在一段加粗、下划线的文本时，菜单栏里的 B 和 U 被激活，如下图
    // 2. 什么时候执行这个函数？每次编辑器区域的选区变化（如鼠标操作、键盘操作等），都会触发各个菜单的 tryChangeActive 函数，重新计算菜单的激活状态
    tryChangeActive() {
        // 激活菜单
        // 1. 菜单 DOM 节点会增加一个 .w-e-active 的 css class
        // 2. this.this.isActive === true
        this.active()

        // // 取消激活菜单
        // // 1. 菜单 DOM 节点会删掉 .w-e-active
        // // 2. this.this.isActive === false
        // this.unActive()
    }
}
