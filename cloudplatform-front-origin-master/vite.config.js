import path from "path";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import fs from "fs";
import dotenv from "dotenv";
import { svgBuilder } from './src/utils/svgBuilder';

// https://vitejs.dev/config/
export default () => {
  let NODE_ENV = process.env.NODE_ENV || 'development';
  const envConfig = dotenv.parse(fs.readFileSync(`.env.${NODE_ENV}`))
  for (const k in envConfig) {
    process.env[k] = envConfig[k]
  }
  return defineConfig({
    resolve: {
      alias: {
        "~/": `${path.resolve(__dirname, "src")}/`,
      },
    },
    define: {
      'process.env': {
        ...process.env
      },
      __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: 'true'
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "~/styles/element/index.scss" as *;`,
        },
      },
    },
    plugins: [
      vue(),
      svgBuilder('./src/assets/svg/'),
    ],
    // 服务器端口号
    server: {
      hmr: true,
      /* 自动打开浏览器 */
      open: true,
      /* 设置为0.0.0.0则所有的地址均能访问 */
      host: '0.0.0.0',
      /* 端口号 */
      port: 9000,
      // 设置代理
      proxy: {
        '/neuedu': {
          target: 'http://localhost:8082', // 部署地址
          changeOrigin: true,
          rewrite: path => path.replace(/^\/neuedu/, '')
        },
        '/content': {
          target: 'http://staging.ct.neuedu.com', // 部署地址
          changeOrigin: true,
        }
      },
      disableHostCheck: true
    }
  })
};
