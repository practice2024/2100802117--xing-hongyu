import request from '@/utils/request'

// 查询云服务列表
export function listServe(query) {
  return request({
    url: '/serve/serve/list',
    method: 'get',
    params: query
  })
}

// 查询云服务详细
export function getServe(serId) {
  return request({
    url: '/serve/serve/' + serId,
    method: 'get'
  })
}

// 新增云服务
export function addServe(data) {
  return request({
    url: '/serve/serve',
    method: 'post',
    data: data
  })
}

// 修改云服务
export function updateServe(data) {
  return request({
    url: '/serve/serve',
    method: 'put',
    data: data
  })
}

// 删除云服务
export function delServe(serId) {
  return request({
    url: '/serve/serve/' + serId,
    method: 'delete'
  })
}
