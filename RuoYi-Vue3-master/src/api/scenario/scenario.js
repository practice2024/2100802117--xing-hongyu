import request from '@/utils/request'

// 查询云场景列表
export function listScenario(query) {
  return request({
    url: '/scenario/scenario/list',
    method: 'get',
    params: query
  })
}

// 查询云场景详细
export function getScenario(sceId) {
  return request({
    url: '/scenario/scenario/' + sceId,
    method: 'get'
  })
}

// 新增云场景
export function addScenario(data) {
  return request({
    url: '/scenario/scenario',
    method: 'post',
    data: data
  })
}

// 修改云场景
export function updateScenario(data) {
  return request({
    url: '/scenario/scenario',
    method: 'put',
    data: data
  })
}

// 删除云场景
export function delScenario(sceId) {
  return request({
    url: '/scenario/scenario/' + sceId,
    method: 'delete'
  })
}

// 审核通过or不通过云场景
export function isApproveScenario(data) {
  return request({
    url: '/scenario/scenario/isApprove',
    method: 'put',
    data: data
  })
}
// 批量审核通过云场景
export function passApproveScenario(id) {
   return request({
   url: '/scenario/scenario/passApprove/' + id,
   method: 'put'
   })
  }
  // 提交审核云场景
export function approveScenario(id) {
  return request({
  url: '/scenario/scenario/approve/' + id,
   method: 'put'
})
  }
