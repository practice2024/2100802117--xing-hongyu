import request from '@/utils/request'

// 查询消息管理列表
export function listMessage(query) {
  return request({
    url: '/message/message/list',
    method: 'get',
    params: query
  })
}

// 查询消息管理详细
export function getMessage(msgId) {
  return request({
    url: '/message/message/' + msgId,
    method: 'get'
  })
}

// 新增消息管理
export function addMessage(data) {
  return request({
    url: '/message/message',
    method: 'post',
    data: data
  })
}

// 修改消息管理
export function updateMessage(data) {
  return request({
    url: '/message/message',
    method: 'put',
    data: data
  })
}
// 删除消息管理
export function delMessage(msgId) {
  return request({
    url: '/message/message/' + msgId,
    method: 'delete'
  })
}
// 审核通过or不通过申请消息
export function isApproveMessage(data) {
  return request({
    url: '/message/message/isApprove',
    method: 'put',
    data: data
  })
}