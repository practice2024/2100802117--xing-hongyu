import request from '@/utils/request'

// 查询机构管理列表（无鉴权）
export function listOrgnization(query) {
  return request({
    url: '/api/orgnization/list',
    method: 'get',
    params: query
  })
}
