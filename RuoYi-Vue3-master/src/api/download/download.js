import request from '@/utils/request'

// 查询资料下载列表
export function listDownload(query) {
  return request({
    url: '/download/download/list',
    method: 'get',
    params: query
  })
}

// 查询资料下载详细
export function getDownload(downId) {
  return request({
    url: '/download/download/' + downId,
    method: 'get'
  })
}

// 新增资料下载
export function addDownload(data) {
  return request({
    url: '/download/download',
    method: 'post',
    data: data
  })
}

// 修改资料下载
export function updateDownload(data) {
  return request({
    url: '/download/download',
    method: 'put',
    data: data
  })
}

// 删除资料下载
export function delDownload(downId) {
  return request({
    url: '/download/download/' + downId,
    method: 'delete'
  })
}
