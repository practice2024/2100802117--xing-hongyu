package com.ruoyi.scenario.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.scenario.domain.DemoScenario;

/**
 * 云场景Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IDemoScenarioService 
{
    /**
     * 查询云场景
     * 
     * @param sceId 云场景主键
     * @return 云场景
     */
    public DemoScenario selectDemoScenarioBySceId(Long sceId);

    /**
     * 查询云场景列表
     * 
     * @param demoScenario 云场景
     * @return 云场景集合
     */
    public List<DemoScenario> selectDemoScenarioList(DemoScenario demoScenario);

    /**
     * 新增云场景
     * 
     * @param demoScenario 云场景
     * @return 结果
     */
    public int insertDemoScenario(DemoScenario demoScenario);

    /**
     * 修改云场景
     * 
     * @param demoScenario 云场景
     * @return 结果
     */
    public int updateDemoScenario(DemoScenario demoScenario);

    /**
     * 批量删除云场景
     * 
     * @param sceIds 需要删除的云场景主键集合
     * @return 结果
     */
    public int deleteDemoScenarioBySceIds(Long[] sceIds);

    /**
     * 删除云场景信息
     * 
     * @param sceId 云场景主键
     * @return 结果
     */
    public int deleteDemoScenarioBySceId(Long sceId);
    public int updateScenarioByIds(Map<String, Object> params);

}
