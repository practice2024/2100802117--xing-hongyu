package com.ruoyi.scenario.controller;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.scenario.domain.DemoScenario;
import com.ruoyi.scenario.service.IDemoScenarioService;
import com.ruoyi.web.controller.common.Const;
import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@Api(tags = "云场景接口")
@Anonymous
@RestController
@RequestMapping("/api/scenario")

public class DemoScenarioApiController extends BaseController {
    @Autowired
    private IDemoScenarioService demoScenarioService;
    @Autowired
    private RedisCache redisCache;

    @ApiOperation(value = "获取云场景详细信息",response = ArrayList.class)
    @ApiImplicitParam(name = "id", value = "云场景ID", required = true, dataType = "long", paramType = "path", dataTypeClass = Long.class)
    @GetMapping(value = "/{id}")

    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        DemoScenario demoScenario = demoScenarioService.selectDemoScenarioBySceId(id);
        Long reads = redisCache.getCacheObject(Const.SCENARIO_READS+id);
        if(reads==null){
            reads = demoScenario.getSceReads();
        }
        redisCache.setCacheObject(Const.SCENARIO_READS+id,reads+1);
        demoScenario.setSceReads(reads+1);
        return AjaxResult.success(demoScenario);
    }
    @ApiOperation(value = "获取云场景列表",response = ArrayList.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "组织ID", dataType = "long", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "orgName", value = "组织名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sceMainTitle", value = "云场景名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sceTag", value = "云场景标签名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sceStatus", value = "审核状态", dataType = "String", dataTypeClass = String.class)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = DemoScenario.class)
    })
    @GetMapping("/list")
    public TableDataInfo list(DemoScenario demoScenario)
    {
        startPage();
        demoScenario.setSceStatus(Const.SCE_STATUS_OPEN);
        List<DemoScenario> list = demoScenarioService.selectDemoScenarioList(demoScenario);
        if(list!=null){
            for (DemoScenario ds:list){
                Long reads = redisCache.getCacheObject(Const.SCENARIO_READS + ds.getSceId());
                if (reads != null) {
                    ds.setSceReads(reads);
                }
            }
        }

        return getDataTable(list);
    }

}