package com.ruoyi.serve.controller;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.scenario.service.IDemoScenarioService;
import com.ruoyi.serve.domain.DemoServe;
import com.ruoyi.serve.service.IDemoServeService;
import com.ruoyi.web.controller.common.Const;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @projectName: RuoYi-Vue-master
 * @package: com.ruoyi.serve.controller
 * @className: DemoServeApiController
 * @author: duruijuan
 * @description:
 * @since: 2024-07-04 10:55
 * @version: 1.0
 */
@Api(tags = "云服务接口")
@Anonymous
@RestController
@RequestMapping("/api/serve")
public class DemoServeApiController extends BaseController {
    @Autowired
    private IDemoServeService demoServeService;
    @Autowired
    private RedisCache redisCache;
    @ApiOperation(value = "获取云服务列表",response = ArrayList.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "组织ID", dataType = "long", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "orgName", value = "组织名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "serName", value = "云服务名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "serType", value = "云服务类型名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "serStatus", value = "审核状态", dataType = "String", dataTypeClass = String.class)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = DemoServe.class)
    })
    @GetMapping("/list")
    public TableDataInfo list(DemoServe demoServe)
    {
        startPage();
        demoServe.setSerStatus(Const.SCE_STATUS_OPEN);
        List<DemoServe> list = demoServeService.selectDemoServeList(demoServe);
        if(list!=null) {
            for(DemoServe ds:list) {
                Long reads = redisCache.getCacheObject(Const.SCENARIO_READS + ds.getSerId());
                if (reads != null) {
                    ds.setSerReads(reads);
                }
            }
        }
        return getDataTable(list);
    }
    /**
     * 获取云服务详细信息
     */
    @ApiOperation(value = "获取云服务详细信息",response = ArrayList.class)
    @ApiImplicitParam(name = "id", value = "云服务ID", required = true, dataType = "long", paramType = "path", dataTypeClass = Long.class)
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        DemoServe demoServe = demoServeService.selectDemoServeBySerId(id);        return AjaxResult.success(demoServe);
    }
}