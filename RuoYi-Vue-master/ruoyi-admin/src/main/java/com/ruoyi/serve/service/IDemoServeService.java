package com.ruoyi.serve.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.serve.domain.DemoServe;

/**
 * 云服务Service接口
 *
 * @author ruoyi
 * @date 2024-07-04
 */
public interface IDemoServeService
{
    /**
     * 查询云服务
     *
     * @param serId 云服务主键
     * @return 云服务
     */
    public DemoServe selectDemoServeBySerId(Long serId);

    /**
     * 查询云服务列表
     *
     * @param demoServe 云服务
     * @return 云服务集合
     */
    public List<DemoServe> selectDemoServeList(DemoServe demoServe);

    /**
     * 新增云服务
     *
     * @param demoServe 云服务
     * @return 结果
     */
    public int insertDemoServe(DemoServe demoServe);

    /**
     * 修改云服务
     *
     * @param demoServe 云服务
     * @return 结果
     */
    public int updateDemoServe(DemoServe demoServe);

    /**
     * 批量删除云服务
     *
     * @param serIds 需要删除的云服务主键集合
     * @return 结果
     */
    public int deleteDemoServeBySerIds(Long[] serIds);

    /**
     * 删除云服务信息
     *
     * @param serId 云服务主键
     * @return 结果
     */
    public int deleteDemoServeBySerId(Long serId);

    int updateServeByIds(Map<String, Object> params);
}