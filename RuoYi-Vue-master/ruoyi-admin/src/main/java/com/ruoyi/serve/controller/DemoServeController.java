package com.ruoyi.serve.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.controller.common.Const;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.serve.domain.DemoServe;
import com.ruoyi.serve.service.IDemoServeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 云服务Controller
 *
 * @author ruoyi
 * @date 2024-07-02
 */
@RestController
@RequestMapping("/serve/serve")
public class DemoServeController extends BaseController
{
    @Autowired
    private IDemoServeService demoServeService;

    /**
     * 查询云服务列表
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:list')")
    @GetMapping("/list")
    public TableDataInfo list(DemoServe demoServe)
    {
        startPage();
        List<DemoServe> list = demoServeService.selectDemoServeList(demoServe);
        return getDataTable(list);
    }

    /**
     * 导出云服务列表
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:export')")
    @Log(title = "云服务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoServe demoServe)
    {
        List<DemoServe> list = demoServeService.selectDemoServeList(demoServe);
        ExcelUtil<DemoServe> util = new ExcelUtil<DemoServe>(DemoServe.class);
        util.exportExcel(response, list, "云服务数据");
    }

    /**
     * 获取云服务详细信息
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:query')")
    @GetMapping(value = "/{serId}")
    public AjaxResult getInfo(@PathVariable("serId") Long serId)
    {
        return success(demoServeService.selectDemoServeBySerId(serId));
    }

    /**
     * 新增云服务
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:add')")
    @Log(title = "云服务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DemoServe demoServe)
    {
        demoServe.setCreateUserId(getUserId());
        return toAjax(demoServeService.insertDemoServe(demoServe));
    }

    /**
     * 修改云服务
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:edit')")
    @Log(title = "云服务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DemoServe demoServe)
    {
        return toAjax(demoServeService.updateDemoServe(demoServe));
    }

    /**
     * 删除云服务
     */
    @PreAuthorize("@ss.hasPermi('serve:serve:remove')")
    @Log(title = "云服务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{serIds}")
    public AjaxResult remove(@PathVariable Long[] serIds)
    {
        return toAjax(demoServeService.deleteDemoServeBySerIds(serIds));
    }
    /**
     * 審核通過or不通過云服务
     */
    @PreAuthorize("@ss.hasPermi('expo:serve:audit')")
    @Log(title = "云服务", businessType = BusinessType.UPDATE)
    @PutMapping("/isApprove")
    public AjaxResult isApprove(@RequestBody DemoServe demoServe)
    {
        Map<String,Object> params = new HashMap<String,Object>();
        Long[] ids=new Long[]{demoServe.getSerId()};
        params.put("ids", ids);
        String status=demoServe.getSerUnapprovedReason()==null||"".equals(demoServe.getSerUnapprovedReason())? Const.SCE_STATUS_OPEN: Const.SCE_STATUS_UNAPPROVED;
        params.put("serStatus", status);
        params.put("serApprovedUserId", this.getUserId());
        params.put("serApprovedTime", DateUtils.getNowDate());
        params.put("serUnApprovedReason", demoServe.getSerUnapprovedReason());

        int i = demoServeService.updateServeByIds(params);
        if(i>0){
            return AjaxResult.success();
        }else{
            return AjaxResult.error("当前状态是"+(demoServe.getSerStatus()==null?"空":demoServe.getSerStatus())+"，无法进行审核操作");
        }
    }

    /**
     * 批量審核通過云服务
     */
    @PreAuthorize("@ss.hasPermi('expo:serve:passApprove')")
    @Log(title = "云服务", businessType = BusinessType.UPDATE)
    @PutMapping("/passApprove/{ids}")
    public AjaxResult passApprove(@PathVariable Long[] ids)
    {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("ids", ids);
        params.put("serStatus", Const.SCE_STATUS_OPEN);
        params.put("approvedByUserId", this.getUserId());
        params.put("approvedTime", DateUtils.getNowDate());
        params.put("action", "APPROVE");
        int i = demoServeService.updateServeByIds(params);
        if(i>0){
            return AjaxResult.success();
        }else{
            return AjaxResult.error("只有状态为‘审核中’的云场景才能进行审核操作");
        }
    }

    @PreAuthorize("@ss.hasPermi('scenario:scenario:approve')")
    @Log(title = "云服务", businessType = BusinessType.UPDATE)
    @PutMapping("/approve/{ids}")
    public AjaxResult approve(@PathVariable Long[] ids)
    {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("ids", ids);
        params.put("serStatus", Const.SCE_STATUS_SUBMITTED);
        params.put("createUserId",this.getUserId());
        params.put("updateTime",DateUtils.getNowDate());
        params.put("action", "SUBMIT");
        System.out.println("params:"+params);
        int i = demoServeService.updateServeByIds(params);
        if(i>0){
            return AjaxResult.success();
        }else{
            return AjaxResult.error("只有状态为‘布展中’的云场景才能进行提交审核操作");
        }
    }
}