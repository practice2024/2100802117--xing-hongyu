package com.ruoyi.serve.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 云服务对象 demo_serve
 *
 * @author ruoyi
 * @date 2024-07-04
 */
public class DemoServe extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 云服务ID */
    private Long serId;

    /** 机构ID */
    @Excel(name = "机构ID")
    private Long orgId;
    @Excel(name = "机构名称")
    private String orgName;
    /** 名称 */
    @Excel(name = "名称")
    private String serName;

    /** 封面标题 */
    @Excel(name = "封面标题")
    private String serCoverTitle;

    /** 类型 */
    @Excel(name = "类型")
    private String serType;

    /** 图片 */
    @Excel(name = "图片")
    private String serImgUrl;

    /** 阅读量 */
    private Long serReads;

    /** 点赞量 */
    private Long serLikes;

    /** 分享量 */
    private Long serShares;

    /** 状态 */
    @Excel(name = "状态")
    private String serStatus;

    /** 审核用户ID */
    private Long serApprovedUserId;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date serApprovedTime;

    /** 未通过原因 */
    private String serUnapprovedReason;

    /** 创建用户ID */
    private Long createUserId;

    /** 详情 */
    private String serContent;

    public void setSerId(Long serId)
    {
        this.serId = serId;
    }

    public Long getSerId()
    {
        return serId;
    }
    public void setOrgId(Long orgId)
    {
        this.orgId = orgId;
    }

    public Long getOrgId()
    {
        return orgId;
    }
    public void setSerName(String serName)
    {
        this.serName = serName;
    }

    public String getSerName()
    {
        return serName;
    }
    public void setSerCoverTitle(String serCoverTitle)
    {
        this.serCoverTitle = serCoverTitle;
    }

    public String getSerCoverTitle()
    {
        return serCoverTitle;
    }
    public void setSerType(String serType)
    {
        this.serType = serType;
    }

    public String getSerType()
    {
        return serType;
    }
    public void setSerImgUrl(String serImgUrl)
    {
        this.serImgUrl = serImgUrl;
    }

    public String getSerImgUrl()
    {
        return serImgUrl;
    }
    public void setSerReads(Long serReads)
    {
        this.serReads = serReads;
    }

    public Long getSerReads()
    {
        return serReads;
    }
    public void setSerLikes(Long serLikes)
    {
        this.serLikes = serLikes;
    }

    public Long getSerLikes()
    {
        return serLikes;
    }
    public void setSerShares(Long serShares)
    {
        this.serShares = serShares;
    }

    public Long getSerShares()
    {
        return serShares;
    }
    public void setSerStatus(String serStatus)
    {
        this.serStatus = serStatus;
    }

    public String getSerStatus()
    {
        return serStatus;
    }
    public void setSerApprovedUserId(Long serApprovedUserId)
    {
        this.serApprovedUserId = serApprovedUserId;
    }

    public Long getSerApprovedUserId()
    {
        return serApprovedUserId;
    }
    public void setSerApprovedTime(Date serApprovedTime)
    {
        this.serApprovedTime = serApprovedTime;
    }

    public Date getSerApprovedTime()
    {
        return serApprovedTime;
    }
    public void setSerUnapprovedReason(String serUnapprovedReason)
    {
        this.serUnapprovedReason = serUnapprovedReason;
    }

    public String getSerUnapprovedReason()
    {
        return serUnapprovedReason;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setSerContent(String serContent)
    {
        this.serContent = serContent;
    }

    public String getSerContent()
    {
        return serContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("serId", getSerId())
                .append("orgId", getOrgId())
                .append("serName", getSerName())
                .append("serCoverTitle", getSerCoverTitle())
                .append("serType", getSerType())
                .append("serImgUrl", getSerImgUrl())
                .append("serReads", getSerReads())
                .append("serLikes", getSerLikes())
                .append("serShares", getSerShares())
                .append("serStatus", getSerStatus())
                .append("serApprovedUserId", getSerApprovedUserId())
                .append("serApprovedTime", getSerApprovedTime())
                .append("serUnapprovedReason", getSerUnapprovedReason())
                .append("createUserId", getCreateUserId())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("serContent", getSerContent())
                .toString();
    }
}