package com.ruoyi.serve.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.serve.mapper.DemoServeMapper;
import com.ruoyi.serve.domain.DemoServe;
import com.ruoyi.serve.service.IDemoServeService;

/**
 * 云服务Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-04
 */
@Service
public class DemoServeServiceImpl implements IDemoServeService
{
    @Autowired
    private DemoServeMapper demoServeMapper;

    /**
     * 查询云服务
     *
     * @param serId 云服务主键
     * @return 云服务
     */
    @Override
    public DemoServe selectDemoServeBySerId(Long serId)
    {
        return demoServeMapper.selectDemoServeBySerId(serId);
    }

    /**
     * 查询云服务列表
     *
     * @param demoServe 云服务
     * @return 云服务
     */
    @Override
    public List<DemoServe> selectDemoServeList(DemoServe demoServe)
    {
        return demoServeMapper.selectDemoServeList(demoServe);
    }

    /**
     * 新增云服务
     *
     * @param demoServe 云服务
     * @return 结果
     */
    @Override
    public int insertDemoServe(DemoServe demoServe)
    {
        demoServe.setCreateTime(DateUtils.getNowDate());
        return demoServeMapper.insertDemoServe(demoServe);
    }

    /**
     * 修改云服务
     *
     * @param demoServe 云服务
     * @return 结果
     */
    @Override
    public int updateDemoServe(DemoServe demoServe)
    {
        demoServe.setUpdateTime(DateUtils.getNowDate());
        return demoServeMapper.updateDemoServe(demoServe);
    }

    /**
     * 批量删除云服务
     *
     * @param serIds 需要删除的云服务主键
     * @return 结果
     */
    @Override
    public int deleteDemoServeBySerIds(Long[] serIds)
    {
        return demoServeMapper.deleteDemoServeBySerIds(serIds);
    }

    /**
     * 删除云服务信息
     *
     * @param serId 云服务主键
     * @return 结果
     */
    @Override
    public int deleteDemoServeBySerId(Long serId)
    {
        return demoServeMapper.deleteDemoServeBySerId(serId);
    }

    @Override
    public int updateServeByIds(Map<String, Object> params) {
        return demoServeMapper.updateDemoServeByIds(params);
    }
}