package com.ruoyi.download.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.download.mapper.DemoDownloadMapper;
import com.ruoyi.download.domain.DemoDownload;
import com.ruoyi.download.service.IDemoDownloadService;

/**
 * 云下载Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-08
 */
@Service
public class DemoDownloadServiceImpl implements IDemoDownloadService
{
    @Autowired
    private DemoDownloadMapper demoDownloadMapper;

    /**
     * 查询云下载
     *
     * @param downId 云下载主键
     * @return 云下载
     */
    @Override
    public DemoDownload selectDemoDownloadByDownId(Long downId)
    {
        return demoDownloadMapper.selectDemoDownloadByDownId(downId);
    }

    /**
     * 查询云下载列表
     *
     * @param demoDownload 云下载
     * @return 云下载
     */
    @Override
    public List<DemoDownload> selectDemoDownloadList(DemoDownload demoDownload)
    {
        return demoDownloadMapper.selectDemoDownloadList(demoDownload);
    }

    /**
     * 新增云下载
     *
     * @param demoDownload 云下载
     * @return 结果
     */
    @Override
    public int insertDemoDownload(DemoDownload demoDownload)
    {
        return demoDownloadMapper.insertDemoDownload(demoDownload);
    }

    /**
     * 修改云下载
     *
     * @param demoDownload 云下载
     * @return 结果
     */
    @Override
    public int updateDemoDownload(DemoDownload demoDownload)
    {
        demoDownload.setUpdateTime(DateUtils.getNowDate());
        return demoDownloadMapper.updateDemoDownload(demoDownload);
    }

    /**
     * 批量删除云下载
     *
     * @param downIds 需要删除的云下载主键
     * @return 结果
     */
    @Override
    public int deleteDemoDownloadByDownIds(Long[] downIds)
    {
        return demoDownloadMapper.deleteDemoDownloadByDownIds(downIds);
    }

    /**
     * 删除云下载信息
     *
     * @param downId 云下载主键
     * @return 结果
     */
    @Override
    public int deleteDemoDownloadByDownId(Long downId)
    {
        return demoDownloadMapper.deleteDemoDownloadByDownId(downId);
    }
}
