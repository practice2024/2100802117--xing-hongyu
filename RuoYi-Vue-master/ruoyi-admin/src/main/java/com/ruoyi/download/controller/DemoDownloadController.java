package com.ruoyi.download.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.download.domain.DemoDownload;
import com.ruoyi.download.service.IDemoDownloadService;
import com.ruoyi.web.controller.common.Const;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 云下载Controller
 *
 * @author ruoyi
 * @date 2024-07-08
 */
@RestController
@RequestMapping("/download/download")
public class DemoDownloadController extends BaseController
{
    @Autowired
    private IDemoDownloadService demoDownloadService;

    /**
     * 查询资料下载列表
     */
    @PreAuthorize("@ss.hasPermi('download:download:list')")
    @GetMapping("/list")
    public TableDataInfo list(DemoDownload demoDownload)
    {
        startPage();
        List<DemoDownload> list = demoDownloadService.selectDemoDownloadList(demoDownload);
        return getDataTable(list);
    }

    /**
     * 导出资料下载列表
     */
    @PreAuthorize("@ss.hasPermi('download:download:export')")
    @Log(title = "资料下载", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoDownload demoDownload)
    {
        List<DemoDownload> list = demoDownloadService.selectDemoDownloadList(demoDownload);
        ExcelUtil<DemoDownload> util = new ExcelUtil<DemoDownload>(DemoDownload.class);
        util.exportExcel(response, list, "资料下载数据");
    }

    /**
     * 获取资料下载详细信息
     */
    @PreAuthorize("@ss.hasPermi('download:download:query')")
    @GetMapping(value = "/{downId}")
    public AjaxResult getInfo(@PathVariable("downId") Long downId)
    {
        return success(demoDownloadService.selectDemoDownloadByDownId(downId));
    }

    /**
     * 新增资料下载
     */
    @PreAuthorize("@ss.hasPermi('download:download:add')")
    @Log(title = "资料下载", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DemoDownload demoDownload)
    {
        return toAjax(demoDownloadService.insertDemoDownload(demoDownload));
    }

    /**
     * 修改资料下载
     */
    @PreAuthorize("@ss.hasPermi('download:download:edit')")
    @Log(title = "资料下载", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DemoDownload demoDownload)
    {
        return toAjax(demoDownloadService.updateDemoDownload(demoDownload));
    }

    /**
     * 删除资料下载
     */
    @PreAuthorize("@ss.hasPermi('download:download:remove')")
    @Log(title = "资料下载", businessType = BusinessType.DELETE)
    @DeleteMapping("/{downIds}")
    public AjaxResult remove(@PathVariable Long[] downIds)
    {
        return toAjax(demoDownloadService.deleteDemoDownloadByDownIds(downIds));
    }

//
//    @ApiOperation(value = "获取资料下载列表",response = ArrayList.class)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "downId", value = "文件ID", dataType = "long", dataTypeClass = Long.class),
//            @ApiImplicitParam(name = "downTitle", value = "文件标题", dataType = "String", dataTypeClass = String.class),
//            @ApiImplicitParam(name = "downUrl", value = "文件Url", dataType = "String", dataTypeClass = String.class),
//            @ApiImplicitParam(name = "downStatus", value = "文件状态", dataType = "String", dataTypeClass = String.class)
//    })
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "ok", response = DemoDownload.class)
//    })
//    @GetMapping("/list")
//	public TableDataInfo list(DemoDownload demoDownload)
//    {
//        startPage();
//        demoDownload.setDownStatus(Const.STATUS_NORMAL);
//        List<DemoDownload> list = demoDownloadService.selectDemoDownloadList(demoDownload);
//        return getDataTable(list);
//    }
}
