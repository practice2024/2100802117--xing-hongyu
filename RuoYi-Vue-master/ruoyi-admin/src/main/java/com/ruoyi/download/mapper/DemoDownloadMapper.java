package com.ruoyi.download.mapper;

import java.util.List;
import com.ruoyi.download.domain.DemoDownload;

/**
 * 云下载Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-08
 */
public interface DemoDownloadMapper
{
    /**
     * 查询云下载
     *
     * @param downId 云下载主键
     * @return 云下载
     */
    public DemoDownload selectDemoDownloadByDownId(Long downId);

    /**
     * 查询云下载列表
     *
     * @param demoDownload 云下载
     * @return 云下载集合
     */
    public List<DemoDownload> selectDemoDownloadList(DemoDownload demoDownload);

    /**
     * 新增云下载
     *
     * @param demoDownload 云下载
     * @return 结果
     */
    public int insertDemoDownload(DemoDownload demoDownload);

    /**
     * 修改云下载
     *
     * @param demoDownload 云下载
     * @return 结果
     */
    public int updateDemoDownload(DemoDownload demoDownload);

    /**
     * 删除云下载
     *
     * @param downId 云下载主键
     * @return 结果
     */
    public int deleteDemoDownloadByDownId(Long downId);

    /**
     * 批量删除云下载
     *
     * @param downIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDemoDownloadByDownIds(Long[] downIds);
}
