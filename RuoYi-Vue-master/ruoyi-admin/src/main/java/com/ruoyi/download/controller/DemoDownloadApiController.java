package com.ruoyi.download.controller;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.download.domain.DemoDownload;
import com.ruoyi.download.service.IDemoDownloadService;
import com.ruoyi.web.controller.common.Const;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "资料下载接口")
@Anonymous
@RestController
@RequestMapping("/api/download")
public class DemoDownloadApiController extends BaseController{
    @Autowired
    private IDemoDownloadService demoDownloadService;

    @ApiOperation(value = "获取资料下载列表",response = ArrayList.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "downId", value = "文件ID", dataType = "long", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "downTitle", value = "文件标题", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "downUrl", value = "文件Url", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "downStatus", value = "文件状态", dataType = "String", dataTypeClass = String.class)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = DemoDownload.class)
    })
    @GetMapping("/list")
    public TableDataInfo list(DemoDownload demoDownload)
    {
        startPage();
        demoDownload.setDownStatus(Const.STATUS_NORMAL);
        List<DemoDownload> list = demoDownloadService.selectDemoDownloadList(demoDownload);
        return getDataTable(list);
    }

}
