package com.ruoyi.web.controller.common;

public class Const {
    public static final String SCE_STATUS_NEW = "NEW";//布展中
    public static final String SCE_STATUS_SUBMITTED = "SUBMITTED";//审核中
    public static final String SCE_STATUS_OPEN = "OPEN";//审核通过，已开展
    public static final String SCE_STATUS_UNAPPROVED = "UNAPPROVED";//审核未通过
    public static final String SERVE_READS = "SERVE_READS:";//云服务阅读量
    public static final String SCENARIO_READS = "SCENARIO_READS:";//云场景阅读量
    public static final String ORIGIN_PASSWORD = "123456";//初始密码
    public static final Long ADMIN_ROLE_ID = 1L;//超级管理员角色ID
    public static final Long ORGNIZER_ROLE_ID = 100L;//组织管理员ID
    public static final Long ORGUSER_ROLE_ID = 101L;//机构用户角色ID
    public static final CharSequence ORGUSER_ROLE_NAME = "机构用户";//机构用户角色名
    public static final String MSG_STATUS_APPROVED = "MSG_STATUS_APPROVED";
    public static final String MSG_STATUS_UNAPPROVED = "MSG_STATUS_UNAPPROVED";
    public static final String STATUS_NORMAL = "0";
    public static final String STATUS_CLOSED = "1";//关闭


}