package com.ruoyi.message.service;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.message.domain.DemoMessage;

/**
 * 消息管理Service接口
 *
 * @author ruoyi
 * @date 2024-07-05
 */
public interface IDemoMessageService
{
    /**
     * 查询消息管理
     *
     * @param msgId 消息管理主键
     * @return 消息管理
     */
    public DemoMessage selectDemoMessageByMsgId(Long msgId);

    /**
     * 查询消息管理列表
     *
     * @param demoMessage 消息管理
     * @return 消息管理集合
     */
    public List<DemoMessage> selectDemoMessageList(DemoMessage demoMessage);

    /**
     * 新增消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    public int insertDemoMessage(DemoMessage demoMessage);

    /**
     * 修改消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    public int updateDemoMessage(DemoMessage demoMessage);

    /**
     * 批量删除消息管理
     *
     * @param msgIds 需要删除的消息管理主键集合
     * @return 结果
     */
    public int deleteDemoMessageByMsgIds(Long[] msgIds);

    /**
     * 删除消息管理信息
     *
     * @param msgId 消息管理主键
     * @return 结果
     */
    public int deleteDemoMessageByMsgId(Long msgId);
    int regUserAndUpdateDemoMessage(DemoMessage demoMessage);

    public boolean checkUserNameUnique(SysUser user);
}