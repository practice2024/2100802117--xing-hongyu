package com.ruoyi.message.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;

import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.common.Const;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.message.mapper.DemoMessageMapper;
import com.ruoyi.message.domain.DemoMessage;
import com.ruoyi.message.service.IDemoMessageService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 消息管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-03
 */
@Service
public class DemoMessageServiceImpl implements IDemoMessageService
{
    @Autowired
    private DemoMessageMapper demoMessageMapper;

    @Autowired
    private ISysUserService userService;
    /**
     * 查询消息管理
     *
     * @param msgId 消息管理主键
     * @return 消息管理
     */
    @Override
    public DemoMessage selectDemoMessageByMsgId(Long msgId)
    {
        return demoMessageMapper.selectDemoMessageByMsgId(msgId);
    }

    /**
     * 查询消息管理列表
     *
     * @param demoMessage 消息管理
     * @return 消息管理
     */
    @Override
    public List<DemoMessage> selectDemoMessageList(DemoMessage demoMessage)
    {
        return demoMessageMapper.selectDemoMessageList(demoMessage);
    }

    /**
     * 新增消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    @Override
    public int insertDemoMessage(DemoMessage demoMessage)
    {
        return demoMessageMapper.insertDemoMessage(demoMessage);
    }

    /**
     * 修改消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    @Override
    public int updateDemoMessage(DemoMessage demoMessage)
    {
        return demoMessageMapper.updateDemoMessage(demoMessage);
    }

    /**
     * 批量删除消息管理
     *
     * @param msgIds 需要删除的消息管理主键
     * @return 结果
     */
    @Override
    public int deleteDemoMessageByMsgIds(Long[] msgIds)
    {
        return demoMessageMapper.deleteDemoMessageByMsgIds(msgIds);
    }

    /**
     * 删除消息管理信息
     *
     * @param msgId 消息管理主键
     * @return 结果
     */
    @Override
    public int deleteDemoMessageByMsgId(Long msgId)
    {
        return demoMessageMapper.deleteDemoMessageByMsgId(msgId);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public int regUserAndUpdateDemoMessage(DemoMessage demoMessage) {


        if(Const.MSG_STATUS_APPROVED.equals(demoMessage.getMsgStatus())){
//审核通过，添加用户，分配用户权限
            SysUser user = new SysUser();
            user.setUserName(demoMessage.getUserName());
            user.setNickName(demoMessage.getNickName());
            user.setEmail(demoMessage.getEmail());
            user.setPhonenumber(demoMessage.getPhonenumber());
            user.setPassword(SecurityUtils.encryptPassword(Const.ORIGIN_PASSWORD));
            user.setCreateBy(SecurityUtils.getUsername());
            if(userService.registerUser(user)) {
                Long[] roleIds = new Long[] {Const.ORGUSER_ROLE_ID};
                userService.insertUserAuth(user.getUserId(), roleIds);
                demoMessage.setUserId(user.getUserId());
            }else {
                return -1;
            }
        }

        return demoMessageMapper.updateDemoMessage(demoMessage);
    }
    @Override
    public boolean checkUserNameUnique(SysUser user) {
        // TODO Auto-generated method stub
        DemoMessage info = demoMessageMapper.selectDemoMessageByUserName(user.getUserName());
        if (StringUtils.isNotNull(info))
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}