package com.ruoyi.message.mapper;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.ruoyi.message.domain.DemoMessage;

/**
 * 消息管理Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-05
 */
public interface DemoMessageMapper
{
    /**
     * 查询消息管理
     *
     * @param msgId 消息管理主键
     * @return 消息管理
     */
    public DemoMessage selectDemoMessageByMsgId(Long msgId);

    /**
     * 查询消息管理列表
     *
     * @param demoMessage 消息管理
     * @return 消息管理集合
     */
    public List<DemoMessage> selectDemoMessageList(DemoMessage demoMessage);

    /**
     * 新增消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    public int insertDemoMessage(DemoMessage demoMessage);

    /**
     * 修改消息管理
     *
     * @param demoMessage 消息管理
     * @return 结果
     */
    public int updateDemoMessage(DemoMessage demoMessage);

    /**
     * 删除消息管理
     *
     * @param msgId 消息管理主键
     * @return 结果
     */
    public int deleteDemoMessageByMsgId(Long msgId);

    /**
     * 批量删除消息管理
     *
     * @param msgIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDemoMessageByMsgIds(Long[] msgIds);

    public DemoMessage selectDemoMessageByUserName(
            @NotBlank(message = "用户账号不能为空") @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符") String userName);
}