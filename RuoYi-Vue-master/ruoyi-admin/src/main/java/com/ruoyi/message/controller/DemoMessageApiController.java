package com.ruoyi.message.controller;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.message.domain.DemoMessage;
import com.ruoyi.message.service.IDemoMessageService;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.ruoyi.common.core.domain.AjaxResult.error;

@Api(tags = "申请消息接口")
@Anonymous
@RestController
@RequestMapping("/api/message")
public class DemoMessageApiController extends BaseController {
    @Autowired
    private ISysUserService userService;

    @Autowired
    private IDemoMessageService demoMessageService;
    /**
     * 新增申请消息
     */

    @ApiOperation(value = "新增申请消息",response = AjaxResult.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = AjaxResult.class)
    })
    @PostMapping
    public AjaxResult add(@RequestBody DemoMessage demoMessage)
    {
        SysUser user = new SysUser();
        user.setUserName(demoMessage.getUserName());
        if (!userService.checkUserNameUnique(user)) {
            return error("申请失败，此用户名已存在");
        } else if(!demoMessageService.checkUserNameUnique(user)) {
            return error("此用户名已经发送过申请，请不要重复发送");
        }
        return toAjax(demoMessageService.insertDemoMessage(demoMessage));
    }
}
