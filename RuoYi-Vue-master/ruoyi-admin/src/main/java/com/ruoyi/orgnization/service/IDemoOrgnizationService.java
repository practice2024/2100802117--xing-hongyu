package com.ruoyi.orgnization.service;

import java.util.List;
import com.ruoyi.orgnization.domain.DemoOrgnization;

/**
 * 机构管理Service接口
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
public interface IDemoOrgnizationService 
{
    /**
     * 查询机构管理
     * 
     * @param orgId 机构管理主键
     * @return 机构管理
     */
    public DemoOrgnization selectDemoOrgnizationByOrgId(Long orgId);

    /**
     * 查询机构管理列表
     * 
     * @param demoOrgnization 机构管理
     * @return 机构管理集合
     */
    public List<DemoOrgnization> selectDemoOrgnizationList(DemoOrgnization demoOrgnization);

    /**
     * 新增机构管理
     * 
     * @param demoOrgnization 机构管理
     * @return 结果
     */
    public int insertDemoOrgnization(DemoOrgnization demoOrgnization);

    /**
     * 修改机构管理
     * 
     * @param demoOrgnization 机构管理
     * @return 结果
     */
    public int updateDemoOrgnization(DemoOrgnization demoOrgnization);

    /**
     * 批量删除机构管理
     * 
     * @param orgIds 需要删除的机构管理主键集合
     * @return 结果
     */
    public int deleteDemoOrgnizationByOrgIds(Long[] orgIds);

    /**
     * 删除机构管理信息
     * 
     * @param orgId 机构管理主键
     * @return 结果
     */
    public int deleteDemoOrgnizationByOrgId(Long orgId);
}
