package com.ruoyi.orgnization.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.orgnization.mapper.DemoOrgnizationMapper;
import com.ruoyi.orgnization.domain.DemoOrgnization;
import com.ruoyi.orgnization.service.IDemoOrgnizationService;

/**
 * 机构管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@Service
public class DemoOrgnizationServiceImpl implements IDemoOrgnizationService 
{
    @Autowired
    private DemoOrgnizationMapper demoOrgnizationMapper;

    /**
     * 查询机构管理
     * 
     * @param orgId 机构管理主键
     * @return 机构管理
     */
    @Override
    public DemoOrgnization selectDemoOrgnizationByOrgId(Long orgId)
    {
        return demoOrgnizationMapper.selectDemoOrgnizationByOrgId(orgId);
    }

    /**
     * 查询机构管理列表
     * 
     * @param demoOrgnization 机构管理
     * @return 机构管理
     */
    @Override
    public List<DemoOrgnization> selectDemoOrgnizationList(DemoOrgnization demoOrgnization)
    {
        return demoOrgnizationMapper.selectDemoOrgnizationList(demoOrgnization);
    }

    /**
     * 新增机构管理
     * 
     * @param demoOrgnization 机构管理
     * @return 结果
     */
    @Override
    public int insertDemoOrgnization(DemoOrgnization demoOrgnization)
    {
        demoOrgnization.setCreateTime(DateUtils.getNowDate());
        return demoOrgnizationMapper.insertDemoOrgnization(demoOrgnization);
    }

    /**
     * 修改机构管理
     * 
     * @param demoOrgnization 机构管理
     * @return 结果
     */
    @Override
    public int updateDemoOrgnization(DemoOrgnization demoOrgnization)
    {
        demoOrgnization.setUpdateTime(DateUtils.getNowDate());
        return demoOrgnizationMapper.updateDemoOrgnization(demoOrgnization);
    }

    /**
     * 批量删除机构管理
     * 
     * @param orgIds 需要删除的机构管理主键
     * @return 结果
     */
    @Override
    public int deleteDemoOrgnizationByOrgIds(Long[] orgIds)
    {
        return demoOrgnizationMapper.deleteDemoOrgnizationByOrgIds(orgIds);
    }

    /**
     * 删除机构管理信息
     * 
     * @param orgId 机构管理主键
     * @return 结果
     */
    @Override
    public int deleteDemoOrgnizationByOrgId(Long orgId)
    {
        return demoOrgnizationMapper.deleteDemoOrgnizationByOrgId(orgId);
    }
}
