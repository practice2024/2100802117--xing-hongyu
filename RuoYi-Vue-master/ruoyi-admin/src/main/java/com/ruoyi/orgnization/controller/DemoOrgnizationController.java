package com.ruoyi.orgnization.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.orgnization.domain.DemoOrgnization;
import com.ruoyi.orgnization.service.IDemoOrgnizationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 机构管理Controller
 * 
 * @author ruoyi
 * @date 2024-06-27
 */
@RestController
@RequestMapping("/orgnization/orgnization")
public class DemoOrgnizationController extends BaseController
{
    @Autowired
    private IDemoOrgnizationService demoOrgnizationService;

    /**
     * 查询机构管理列表
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:list')")
    @GetMapping("/list")
    public TableDataInfo list(DemoOrgnization demoOrgnization)
    {
        startPage();
        List<DemoOrgnization> list = demoOrgnizationService.selectDemoOrgnizationList(demoOrgnization);
        return getDataTable(list);
    }

    /**
     * 导出机构管理列表
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:export')")
    @Log(title = "机构管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoOrgnization demoOrgnization)
    {
        List<DemoOrgnization> list = demoOrgnizationService.selectDemoOrgnizationList(demoOrgnization);
        ExcelUtil<DemoOrgnization> util = new ExcelUtil<DemoOrgnization>(DemoOrgnization.class);
        util.exportExcel(response, list, "机构管理数据");
    }

    /**
     * 获取机构管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:query')")
    @GetMapping(value = "/{orgId}")
    public AjaxResult getInfo(@PathVariable("orgId") Long orgId)
    {
        return success(demoOrgnizationService.selectDemoOrgnizationByOrgId(orgId));
    }

    /**
     * 新增机构管理
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:add')")
    @Log(title = "机构管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DemoOrgnization demoOrgnization)
    {
        return toAjax(demoOrgnizationService.insertDemoOrgnization(demoOrgnization));
    }

    /**
     * 修改机构管理
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:edit')")
    @Log(title = "机构管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DemoOrgnization demoOrgnization)
    {
        return toAjax(demoOrgnizationService.updateDemoOrgnization(demoOrgnization));
    }

    /**
     * 删除机构管理
     */
    @PreAuthorize("@ss.hasPermi('orgnization:orgnization:remove')")
    @Log(title = "机构管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orgIds}")
    public AjaxResult remove(@PathVariable Long[] orgIds)
    {
        return toAjax(demoOrgnizationService.deleteDemoOrgnizationByOrgIds(orgIds));
    }
}
