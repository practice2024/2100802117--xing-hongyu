package com.ruoyi.orgnization.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.orgnization.domain.DemoOrgnization;
import com.ruoyi.orgnization.service.IDemoOrgnizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@Api(tags = "机构接口")
@Anonymous
@RestController
@RequestMapping("/api/orgnization")
public class DemoOrgnizationApiController extends BaseController{
    @Autowired
    private IDemoOrgnizationService demoOrgnizationService;
    @ApiOperation(value = "获取机构列表",response = ArrayList.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "机构ID", dataType = "long", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "orgName", value = "机构名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "orgType", value = "机构类型", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "orgWebsite", value = "机构网站", dataType = "String", dataTypeClass = String.class)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = DemoOrgnization.class)})
    @GetMapping("/list")
    public TableDataInfo list(DemoOrgnization demoOrgnization)
    {
        startPage();
        List<DemoOrgnization> list = demoOrgnizationService.selectDemoOrgnizationList(demoOrgnization);
        return getDataTable(list);
    }
}