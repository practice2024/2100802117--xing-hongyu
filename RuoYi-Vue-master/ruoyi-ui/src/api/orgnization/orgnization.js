import request from '@/utils/request'

// 查询机构管理列表
export function listOrgnization(query) {
  return request({
    url: '/orgnization/orgnization/list',
    method: 'get',
    params: query
  })
}

// 查询机构管理详细
export function getOrgnization(orgId) {
  return request({
    url: '/orgnization/orgnization/' + orgId,
    method: 'get'
  })
}

// 新增机构管理
export function addOrgnization(data) {
  return request({
    url: '/orgnization/orgnization',
    method: 'post',
    data: data
  })
}

// 修改机构管理
export function updateOrgnization(data) {
  return request({
    url: '/orgnization/orgnization',
    method: 'put',
    data: data
  })
}

// 删除机构管理
export function delOrgnization(orgId) {
  return request({
    url: '/orgnization/orgnization/' + orgId,
    method: 'delete'
  })
}
